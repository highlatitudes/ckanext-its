from setuptools import setup, find_packages
import sys, os

version = '0.1'

setup(
	name='ckanext-its',
	version=version,
	description="ITS Extension",
	long_description="""\
	""",
	classifiers=[], # Get strings from http://pypi.python.org/pypi?%3Aaction=list_classifiers
	keywords='',
	author='Philippe Duchesne',
	author_email='phd@highlatitud.es',
	url='data.its.be',
	license='',
	packages=find_packages(exclude=['ez_setup', 'examples', 'tests']),
	namespace_packages=['ckanext', 'ckanext.its'],
	include_package_data=True,
	zip_safe=False,
	install_requires=[
		# -*- Extra requirements: -*-
	],
	entry_points=\
	"""
        [ckan.plugins]
	# Add plugins here, eg
	its_extension=ckanext.its.plugin:ITSExtension
	""",
)
