===================================================
ckanext-its - CKAN profile for data.its.be
===================================================

This extension defines one plugin to activate ITS specific features

* ITSExtension (``its_extension``)

Installation
============
1. Install the extension into your python environment.


   For a production site, use the `stable` branch, unless there is a specific
   branch that targets the CKAN core version that you are using.

   To install the extension directly from github, use::

     (pyenv) $ pip install -e git+https://bitbucket.org/highlatitudes/ckanext-its.git#egg=ckanext-its

2. Make sure the CKAN configuration ini file contains the its main plugin::

    ckan.plugins = its_extension
