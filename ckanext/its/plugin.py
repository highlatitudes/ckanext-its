import os
import inspect
import ckan.plugins as p
import ckan.plugins.toolkit as tk


class ITSExtension(p.SingletonPlugin, tk.DefaultDatasetForm):
    p.implements(p.IConfigurer, inherit=True)
    p.implements(p.IDatasetForm, inherit=False)

    def update_config(self, config):

        config['ckan.favicon'] = '/images/favicon.ico'

        p.toolkit.add_template_directory(config, 'theme/templates')
        p.toolkit.add_public_directory(config, 'theme/public')
        p.toolkit.add_resource('theme/public', 'ckanext-its')

    def is_fallback(self):
        # Return True to register this plugin as the default handler for
        # package types not handled by any other IDatasetForm plugin.
        return True

    def package_types(self):
        # This plugin doesn't handle any special package types, it just
        # registers itself as the default (above).
        return []

    def _modify_package_schema(self, schema):
        # Add custom its_location as extra field
        schema.update({
            'its_location': [tk.get_validator('ignore_missing'),
                            tk.get_converter('convert_to_extras')],
            'its_quality': [ tk.get_validator('is_positive_integer'),
                             tk.get_validator('ignore_missing'),
                             tk.get_converter('convert_to_extras')]
        })

        return schema

    def create_package_schema(self):
        schema = super(ITSExtension, self).create_package_schema()
        schema = self._modify_package_schema(schema)
        return schema

    def update_package_schema(self):
        schema = super(ITSExtension, self).update_package_schema()
        schema = self._modify_package_schema(schema)
        return schema

    def show_package_schema(self):
        schema = super(ITSExtension, self).show_package_schema()

        # Add our its_location field to the dataset schema.
        schema.update({
            'its_location': [tk.get_converter('convert_from_extras'),
                             tk.get_validator('ignore_missing')],
            'its_quality': [tk.get_converter('convert_from_extras'),
                             tk.get_validator('ignore_missing'),
                             tk.get_validator('is_positive_integer')]
        })

        return schema
