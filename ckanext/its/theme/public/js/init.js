$(function() {
    var locSelector = $("#field-location")
    $.getJSON( "/js/cities.json", function( data ) {
        $.each(Object.keys(data).sort(function (a,b) {return parseInt(a) - parseInt(b)}), function(idx, code) {
            var value = data[code]
            locSelector.append($("<option value='"+value+"'>"+value+"</option>"))
        })
        var selectedValue = locSelector.attr("data-selected")
        if (selectedValue) locSelector.val(selectedValue)
    })

    var qualitySelector = $("#field-quality")
    qualitySelector.val(qualitySelector.data('selected'))
})